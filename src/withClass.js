import React, { Component } from "react";

export default class WithClass extends Component {
  constructor(props) {
    super(props);
    this.state = { name: "John", surname: "Snow", width: 0 };
  }
  componentDidMount() {
    this.setState({ width: window.innerWidth });
    document.title = this.state.name + " " + this.state.surname;
  }

  componentDidUpdate() {
    document.title = this.state.name + " " + this.state.surname;
    window.addEventListener("resize", this.widthHandler);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.widthHandler);
  }

  widthHandler = () => {
    this.setState({ width: window.innerWidth });
  };

  nameHandler = e => {
    this.setState({ name: e.target.value });
  };

  surnameHandler = e => {
    this.setState({ surname: e.target.value });
  };

  render() {
    return (
      <div className="inner-box with-class">
        <label>Name</label>
        <input
          type="text"
          value={this.state.name}
          onChange={this.nameHandler}
        />
        <label>Surname</label>
        <input
          type="text"
          value={this.state.surname}
          onChange={this.surnameHandler}
        />
        <label>Width</label>
        <label>{this.state.width}</label>
        <label className="user-input">
          {this.state.name.toUpperCase() + " " + this.state.surname.toUpperCase()}
        </label>
      </div>
    );
  }
}
