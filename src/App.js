import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import WithHook from "./withHook";
import WithClass from "./withClass";
import WithCustomHook from "./withCustomHook";
function App() {
  return (
    <div className="box">
      <Router>
        <Route exact path="/withclass" component={WithClass} />
        <Route path="/withhook" component={WithHook} />
        <Route path="/withcusthook" component={WithCustomHook} />
      </Router>
    </div>
  );
}

export default App;
