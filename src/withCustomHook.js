import React, { useState, useEffect, useCallback } from "react";
import "./App.css";
export default function WithCustomHook() {
  const name = useGetInput("John");
  const surname = useGetInput("Snow");
  const width = useWindowWidth();
  const setTitle = useCallback(() => {
    document.title = name.value + " " + surname.value;
  }, [name, surname]);
  useEffect(() => {
    setTitle();
    return () => {};
  }, [setTitle]);

  return (
    <div className="inner-box">
      <label>Name</label>
      <input {...name} />
      <label>Surname</label>
      <input {...surname} />
      <label>Width</label>
      <label>{width}</label>
      <label className="user-input">
        {name.value.toUpperCase() + " " + surname.value.toUpperCase()}
      </label>
    </div>
  );
}

const useGetInput = initialValue => {
  const [value, setValue] = useState(initialValue);
  const valueHandler = e => {
    setValue(e.target.value);
  };
  return { value, onChange: valueHandler };
};

const useWindowWidth = () => {
  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleWidth = () => {
      setWidth(window.innerWidth);
    };
    window.addEventListener("resize", handleWidth);
    return () => {
      window.removeEventListener("resize", handleWidth);
    };
  });
  return width;
};
