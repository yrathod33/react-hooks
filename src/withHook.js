
import React, { useState, useEffect } from "react";
import "./App.css"
export default function HookState() {
  const [name, setName] = useState("john");
  const [surname, setSurname] = useState("snow");
 
  const nameHandler = e => {
    setName(e.target.value);
  };
  const surnameHandler = e => {
    setSurname(e.target.value);
  };
  useEffect(() => {
    document.title = name+" "+surname;
    return () => {};
  });
  const [width, setWidth] = useState(window.innerWidth);
  useEffect(()=>{
      const handleWidth = ()=>{setWidth(window.innerWidth)}
      window.addEventListener('resize',handleWidth);
      return ()=>{
          window.removeEventListener('resize',handleWidth);
      }
  })
  return (
    <div className="inner-box with-hook">
    <label>Name</label>
      <input type="text" value={name} onChange={nameHandler} />
      <label>Surname</label>
      <input type="text" value={surname} onChange={surnameHandler} />
      <label>Width</label>
      <label>{width} px</label>
      <label className="user-input">{name.toUpperCase()+" "+surname.toUpperCase()}</label>
    </div>
  );
}
